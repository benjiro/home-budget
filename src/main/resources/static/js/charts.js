$(document).ready(function () {
    $.ajax({
        url: "/api/trades/main",
        type: "GET",

        contentType: 'application/json; charset=utf-8',
        success: function (resultData) {
            // 0 - food, 1 - clothes, 2 - bills, 3 - entertainment, 4 - other
            var food = "FOOD";
            var clothes = "CLOTHES";
            var bills = "BILLS";
            var entertainment = "ENTERTAINMENT";
            var other = "OTHER";
            var expensesTypesWithValues = [0, 0, 0, 0, 0]
            for (var i = 0; i < resultData.length; i++) {
                if (resultData[i].tradesType === food) {
                    expensesTypesWithValues[0] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === clothes) {
                    expensesTypesWithValues[1] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === bills) {
                    expensesTypesWithValues[2] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === entertainment) {
                    expensesTypesWithValues[3] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === other) {
                    expensesTypesWithValues[4] += parseFloat(resultData[i].amount);
                }
            }
            var ctx = document.getElementById("mainChart").getContext('2d');
            drawCharts(expensesTypesWithValues, ctx, 'Wszystkie wydatki');
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },

        timeout: 120000
    });

    $.ajax({
        url: "/api/trades/month",
        type: "GET",

        contentType: 'application/json; charset=utf-8',
        success: function (resultData) {
            // 0 - food, 1 - clothes, 2 - bills, 3 - entertainment, 4 - other
            var food = "FOOD";
            var clothes = "CLOTHES";
            var bills = "BILLS";
            var entertainment = "ENTERTAINMENT";
            var other = "OTHER";
            var expensesTypesWithValues = [0, 0, 0, 0, 0]
            for (var i = 0; i < resultData.length; i++) {
                if (resultData[i].tradesType === food) {
                    expensesTypesWithValues[0] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === clothes) {
                    expensesTypesWithValues[1] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === bills) {
                    expensesTypesWithValues[2] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === entertainment) {
                    expensesTypesWithValues[3] += parseFloat(resultData[i].amount);
                } else if (resultData[i].tradesType === other) {
                    expensesTypesWithValues[4] += parseFloat(resultData[i].amount);
                }
            }
            var ctx = document.getElementById("monthChart").getContext('2d');
            drawCharts(expensesTypesWithValues, ctx, 'Ostatnie 30 dni');
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },

        timeout: 120000
    });
})

function drawCharts(tradesData, ctx, chartTitle) {
    var chart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Jedzenie", "Ubrania", "Rachunki", "Rozrywka", "Inne"],
            datasets: [
                {
                    data: tradesData,
                    backgroundColor: [
                        "#ff030c",
                        "#fffd02",
                        "#2bff19",
                        "#10dcff",
                        "#4f08ff"
                    ]
                }]
        },
        options: {
            title: {
                display: true,
                text: chartTitle
            }
        }
    })
}