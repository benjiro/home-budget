package com.project.budget.service;

import com.project.budget.domain.Trades;
import com.project.budget.form.TradesCreateForm;
import com.project.budget.repository.TradesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TradesServiceImpl implements TradesService {

    @Autowired
    private TradesRepository tradesRepository;

    @Override
    public Optional<Trades> getTradesById(long id) {
        return tradesRepository.findOneById(id);
    }

    @Override
    public Collection<Trades> getAllTrades() {
        return tradesRepository.findAll();
    }

    @Override
    public List<Trades> getTradesByUserId(long id) {
        return tradesRepository.findAllByUserId(id);
    }

    @Override
    public Trades create(TradesCreateForm form) {
        Trades trades = new Trades();
        trades.setAmount(form.getAmount());
        trades.setDate(form.getDate());
        trades.setDescription(form.getDescription());
        trades.setTradesType(form.getTradesType());
        trades.setUser(form.getUser());
        return tradesRepository.save(trades);
    }

    @Override
    public Page<Trades> findAllByUserId(Pageable pageable, long id) {
        return tradesRepository.findAllByUserId(pageable, id);
    }

    @Override
    public Page<Trades> findAllByUserIdOrderByDateDesc(Pageable pageable, long userId) {
        return tradesRepository.findAllByUserIdOrderByDateDesc(pageable, userId);
    }

    @Override
    public Page<Trades> findAllByUserIdOrderByAmountDesc(Pageable pageable, long userId) {
        return tradesRepository.findAllByUserIdOrderByAmountDesc(pageable, userId);
    }

    @Override
    public Page<Trades> findAllByUserIdOrderByTradesTypeAsc(Pageable pageable, long userId) {
        return tradesRepository.findAllByUserIdOrderByTradesTypeAsc(pageable, userId);
    }

    @Override
    public List<Trades> findAllByUserIdAndDateBetween(long userId, Date startDate, Date endDate) {
        return tradesRepository.findByDateIsBetweenAndUserId(startDate, endDate, userId);
    }

    @Override
    public void removeTradesById(long id) {
        tradesRepository.deleteById(id);
    }
}
