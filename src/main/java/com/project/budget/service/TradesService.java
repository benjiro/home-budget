package com.project.budget.service;

import com.project.budget.domain.Trades;
import com.project.budget.form.TradesCreateForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TradesService {
    Optional<Trades> getTradesById(long id);

    Collection<Trades> getAllTrades();

    List<Trades> getTradesByUserId(long id);

    Trades create(TradesCreateForm form);

    Page<Trades> findAllByUserId(Pageable pageable, long id);

    Page<Trades> findAllByUserIdOrderByDateDesc(Pageable pageable, long userId);

    Page<Trades> findAllByUserIdOrderByAmountDesc(Pageable pageable, long userId);

    Page<Trades> findAllByUserIdOrderByTradesTypeAsc(Pageable pageable, long userId);

    List<Trades> findAllByUserIdAndDateBetween(long userId, Date startDate, Date endDate);

    void removeTradesById(long id);
}
