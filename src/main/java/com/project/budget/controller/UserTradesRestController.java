package com.project.budget.controller;


import com.project.budget.domain.Trades;
import com.project.budget.service.TradesService;
import com.project.budget.service.UserService;
import com.project.budget.utils.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/api")
public class UserTradesRestController {

    private static final Logger logger = Logger.getLogger(UserTradesRestController.class);

    @Autowired
    private TradesService tradesService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/trades/main", method = RequestMethod.GET)
    public @ResponseBody
    List<Trades> listOfUserTrades(HttpServletRequest request) {
        String userEmail = request.getRemoteUser();
        long id = userService.getUserByEmail(userEmail).get().getId();
        return tradesService.getTradesByUserId(id);
    }

    @RequestMapping(value = "/trades/month", method = RequestMethod.GET)
    public @ResponseBody
    List<Trades> listOfUserTradesInLastMonth(HttpServletRequest request) throws ParseException {
        return tradesService.findAllByUserIdAndDateBetween(getLoggedUserId(request),
                DateUtils.asDate(LocalDate.now().minusDays(30)), DateUtils.asDate(LocalDate.now()));
    }

    private long getLoggedUserId(HttpServletRequest request) {
        String userEmail = request.getRemoteUser();
        return userService.getUserByEmail(userEmail).get().getId();
    }
}
