package com.project.budget.controller;

import com.project.budget.domain.Trades;
import com.project.budget.form.TradesCreateForm;
import com.project.budget.service.TradesService;
import com.project.budget.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class TradesController {

    private static final Logger logger = Logger.getLogger(TradesController.class);

    @Autowired
    private TradesService tradesService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/trades", method = RequestMethod.GET)
    public String getTradesCreatePage(HttpServletRequest request, Model model,
                                        @RequestParam(defaultValue = "0") int page) {
        String userEmail = request.getRemoteUser();
        long userId = userService.getUserByEmail(userEmail).get().getId();
        logger.debug(userEmail);

        Page<Trades> tradesPage = tradesService
                .findAllByUserIdOrderByDateDesc(PageRequest.of(page, 10), userId);

        model.addAttribute("trades", new TradesCreateForm());
        model.addAttribute("list", tradesPage);
        return "add_expenses";
    }

    @RequestMapping(value = "/trades", method = RequestMethod.POST)
    public String handleTradesCreateForm(@Valid @ModelAttribute("trades") TradesCreateForm form,
                                           BindingResult bindingResult, HttpServletRequest request) {
        logger.debug(form.getDate().toString());
        if (bindingResult.hasErrors()) {
            return "add_expenses";
        }
        try {
            form.setUser(userService.getUserByEmail(request.getRemoteUser()).get());
            tradesService.create(form);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("email.exists", "Email already exists");
            return "add_expenses";
        }
        return "redirect:/trades";
    }

    @RequestMapping(value = "/trades/delete")
    public String deleteTrades(@RequestParam("id") long id) {
        tradesService.removeTradesById(id);
        return "redirect:/trades";
    }
}
