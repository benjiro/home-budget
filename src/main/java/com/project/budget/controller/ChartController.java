package com.project.budget.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/charts")
public class ChartController {

    @RequestMapping("/")
    public String showChartPage() {
        return "charts";
    }
}
