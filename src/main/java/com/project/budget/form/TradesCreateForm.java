package com.project.budget.form;

import com.project.budget.domain.TradesType;
import com.project.budget.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TradesCreateForm {

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date date = new Date();
    @Digits(integer = 5, fraction = 2)
    private BigDecimal amount = new BigDecimal(0);
    @NotNull
    private TradesType tradesType = TradesType.BILLS;
    private String description = "";
    private User user;
}
