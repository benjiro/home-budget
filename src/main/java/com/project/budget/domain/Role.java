package com.project.budget.domain;

public enum Role {
    USER, ADMIN
}