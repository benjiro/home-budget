package com.project.budget.domain;

public enum TradesType {
    FOOD, CLOTHES, BILLS, ENTERTAINMENT, OTHER, INCOMES
}
