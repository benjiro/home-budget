package com.project.budget.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "trades")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Trades {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Type(type = "date")
    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "trades_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TradesType tradesType;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id")
    private User user;
}
