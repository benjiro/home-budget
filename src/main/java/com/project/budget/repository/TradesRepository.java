package com.project.budget.repository;

import com.project.budget.domain.Trades;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TradesRepository extends JpaRepository<Trades, Long> {

    Optional<Trades> findOneById(long id);

    List<Trades> findAllByUserId(long id);

    Page<Trades> findAllByUserId(Pageable pageable, long id);

    Page<Trades> findAllByUserIdOrderByDateDesc(Pageable pageable, long userId);

    Page<Trades> findAllByUserIdOrderByAmountDesc(Pageable pageable, long userId);

    Page<Trades> findAllByUserIdOrderByTradesTypeAsc(Pageable pageable, long userId);

    List<Trades> findByDateIsBetweenAndUserId(Date startDate, Date endDate, long id);
}
